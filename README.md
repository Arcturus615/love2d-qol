Love2d QOL is a repository containing modules to make using Love2d even easier.

Modules
=======
1. Graphic Module - graphic.lua
  *Contains various simple graphics drawing calls*

2. Vector Module - vector.lua
  *Simple vector class that includes various bits of vector math*

3. Box Module - hitbox.lua
  *Simple box class that can be used for aabb and bounding*

3. Functions Module - functions.lua
  *Contains a few widely usable functions*

4. Color Module - color.lua
  *Returns a table full of various color values*
	*Comes with versions of each color with transparency*

5. Testing Module - testing.lua
  *Currently useless*
	*Will eventually be a library for handling unit testing*

Setup
-----
Simply drop whichever module you want to use in your hierachy and call:

`module = require('module_location')`

If you just want a specific function, use:

`function = require(module_location).function`
